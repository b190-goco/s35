const express = require("express");
const mongoose = require("mongoose");

const app = express();
const port = 3000;

// mongoDB admin
// username: jygsgoco
// password: peanutz11151998
mongoose.connect("mongodb+srv://jygsgoco:peanutz11151998@wdc028-course-booking.ssabc5g.mongodb.net/b190-to-do?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log("We're connected to the database"));

const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		default: "pending"
	}
})

const Task = mongoose.model("Task", taskSchema);

const userSchema = new mongoose.Schema({
	username: String,
	password: String,
})

const User = mongoose.model("User", userSchema);

app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Create a new task
/*
	Business Logic
		- add a functionality that will check if there are duplicate tasks
			- if the task exist, return error
			- if does not exist, add in database
		- the task will be sent from the request body
		- create a new Task object with a "name" field/property
		- the status not required
*/


app.post("/tasks", (req,res) => {
	Task.findOne({name: req.body.name}, (err, result) => {
		if (result != null && result.name != req.body.name) {
			return res.send("Duplicate task found");
		}
		else {
			let newTask = new Task({
				name: req.body.name
			});
			newTask.save((saveErr,savedTask) => {
				if (saveErr) {
					return console.error(saveErr);
				}
				else {
					return res.status(201).send("New task created");
				}
			})
		}
	})
})

app.get("/tasks", (req,res) => {
	Task.find({ },(err,result) => {
		if (err) {
			return console.log(err);
		}
		else {
			return res.status(200).json({
				data: result
			})
		}
	})
})

app.post("/signup", (req,res) => {
	Task.findOne({name: req.body.name}, (err, result) => {
		if (result != null && result.name != req.body.name) {
			return res.send("Duplicate user found");
		}
		else {
			let newUser = new User({
				username: req.body.username,
				password: req.body.password
			});
			newUser.save((saveErr,savedTask) => {
				if (saveErr) {
					return console.error(saveErr);
				}
				else {
					return res.status(201).send(`You have successfully registered, ${req.body.username}`);
				}
			})
		}
	})
})

app.listen(port, () => console.log(`Server running at port: ${port}`));